from django.urls import path
from .views import index, xml, json

urlpatterns = [
    # TODO Add note path using notes_list Views
    path('', index, name='index'),
    path('xml/', xml, name='xml'),
    path('json/', json, name='json'),
]