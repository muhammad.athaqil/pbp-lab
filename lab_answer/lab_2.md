**From what I did on the Lab 02, it is expected that I can determine the difference between JSON, XML, and HTML. Here is what I know.**<br>
*1. What is the difference between JSON and XML?*<br>
JSON : 
- The data provided are string, null, bool, etc.
- It has no tags.
- Can use arrays to represent the data.
- It is data oriented.
- Does not have capacity to display data.<br>

XML : 
- The data is in string format.
- Data represented use tags, for example start tag and end tag.
- Does not contain the concept of array.
- It is document oriented.
- Is a markup language, so it has the capacity to display data.<br>

*2. What is the difference between HTML and XML?*<br>
HTML : 
- It is static.
- Focusses on the appearance of data. Enhances the appearance of text.
- Small errors in the coding can be ignored and the outcome can be achieved.<br>

XML : 
- It is dynamic.
- The main purpose is to focus on the transport of data and saving the data.
- Any error in the code shall not give the final outcome.
