import 'package:flutter/material.dart';

class Info extends StatelessWidget {
  const Info({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Covid-19 Info',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.teal[300],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              child: const Text(
                'Cases in Indonesia',
                style: TextStyle(fontSize: 30, color: Colors.black),
              ),
            ),
            SizedBox(
              child: const Text(
                'Positive : 4251945',
                style: TextStyle(fontSize: 15, color: Colors.black),
              ),
            ),
            SizedBox(
              child: const Text(
                'Hospitalized : 8390',
                style: TextStyle(fontSize: 15, color: Colors.black),
              ),
            ),
            SizedBox(
              child: const Text(
                'Death Cases : 143698',
                style: TextStyle(fontSize: 15, color: Colors.black),
              ),
            ),
            SizedBox(
              child: const Text(
                'Healed : 4099857 ',
                style: TextStyle(fontSize: 15, color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
