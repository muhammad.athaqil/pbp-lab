import 'package:flutter/material.dart';

class Medicine extends StatelessWidget {
  const Medicine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Medicine',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.teal[300],
      ),
      body: BodyLayout(),
    );
  }
}

class BodyLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _myListView(context);
  }

  Widget _myListView(BuildContext context) {
    // backing data
    final medicine = [
      'Vitamin C',
      'Vitamin D',
      'Paracetamol',
      'Oseltamivir',
      'Azithromycin',
      'Zinc',
      'Antibiotic',
      'NaCl 0.9%',
      'CDR',
      'Astria',
    ];

    return ListView.builder(
      itemCount: medicine.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(medicine[index]),
        );
      },
    );
  }
}
