import 'package:flutter/material.dart';

import './screens/info.dart';
import 'screens/medicine.dart';
import 'screens/vaccine.dart';
import 'screens/registerlogin.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: "HeyDoc",
      home: Index(),
      debugShowCheckedModeBanner: false,
    );
  }
}

// main home page
class Index extends StatelessWidget {
  const Index({Key? key})
      : super(
          key: key,
        );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text(
              "HeyDoc",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          backgroundColor: Colors.teal[300],
        ),
        body: servicesButton());
  }
}

class servicesButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _myListView(context);
  }

  @override
  Widget _myListView(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              width: 80,
              height: 50,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.grey[800], // background
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Info()),
                  );
                },
                child: const Text('Info'),
              ),
            ),
            const SizedBox(width: 10),
            SizedBox(
              width: 80,
              height: 50,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.grey[800], // background
                ), // foreground
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Medicine()),
                  );
                },
                child: const Text('Medicine'),
              ),
            ),
            const SizedBox(width: 10),
            SizedBox(
              width: 80,
              height: 50,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.grey[800], // background
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vaccine()),
                  );
                },
                child: const Text('Vaccination'),
              ),
            ),
            const SizedBox(width: 10),
            SizedBox(
              width: 80,
              height: 50,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.grey[800], // background
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginRegister()),
                  );
                },
                child: const Text('Login/Register'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
