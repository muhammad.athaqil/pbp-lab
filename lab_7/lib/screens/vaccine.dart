import 'package:flutter/material.dart';

class Vaccine extends StatelessWidget {
  const Vaccine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Vaccine',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.teal[300],
      ),
      body: BodyLayout(),
    );
  }
}

class BodyLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _myListView(context);
  }

  Widget _myListView(BuildContext context) {
    // backing data
    final clinic = [
      'In Harmony Clinic',
      'Klinik KASIH ProSehat',
      'Balai Besar Pelatihan Kesehatan',
      'Klinik Vaksinasi Vaxcorp',
      'Sentra Vaksin JIEP',
      'Dokter Vaksin',
      'KKP Halim Perdana Kusumah',
      'Tempat Vaksin Jakarta Pusat',
      'Rumah Vaksinasi Pusat',
    ];

    return ListView.builder(
      itemCount: clinic.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(clinic[index]),
        );
      },
    );
  }
}
